'''Samuel Giannotti'''
import sys

import sources
import sinks
import processes

def parse_args():
    if len(sys.argv) != 5:
        print(f"Usage: {sys.argv[0]} <source> <process> <sink> <duration>")
        exit(0)
    source = sys.argv[1]
    process = sys.argv[2]
    sink = sys.argv[3]
    duration = float(sys.argv[4])
    return source,  process, sink, duration


def main():
    source, process, sink, dur = parse_args()

    if source == 'sin':
        sound = sources.sin(duration=dur, freq=400)
    elif source == 'constant':
        sound = sources.constant(duration=dur, positive=True)

        """Añadir la fuente(source)  square"""
    elif source == 'square':
        sound = sources.square(duration=dur, freq=400)
    else:
        sys.exit("Unknown source")


    if process == 'ampli':
        processes.ampli(sound, factor=2)
    elif process == 'reduce':
        processes.reduce(sound, factor=3)
    elif process == 'extend':
        processes.extend(sound, factor=2)
    else:
        sys.exit("Unknown process")

    if sink == "play":
        sinks.play(sound)
    elif sink == "draw":
        sinks.draw(sound=sound, period=0.0001)

        """Añadir el sumidero (sink) store"""
    elif sink == "store":
        sinks.store(sound=sound, path="store.wav")
    else:
        sys.exit("Unknown sink")


if __name__ == '__main__':
    main()
