'''Samuel Giannotti'''
import sys
import sources
import sinks
import processes

def parse_args():
    if len(sys.argv) <6 and len(sys.argv)>10:
        print(f'Usage: {sys.argv[0]} <source> <source_args> <process> <process_args> <sink> <sink_args> <duration>')
        exit(0)
    if len(sys.argv) == 10:
        source = sys.argv[1]
        source_args = sys.argv[2]
        process = [sys.argv[3], sys.argv[5]]
        process_args = [sys.argv[4], sys.argv[6]]
        sink = sys.argv[7]
        sink_args = sys.argv[8]
        duration = float(sys.argv[9])
        return source, source_args, process, process_args, sink, sink_args, duration

    elif len(sys.argv) == 8:
        source = sys.argv[1]
        source_args = sys.argv[2]
        process = sys.argv[3]
        process_args = sys.argv[4]
        sink = sys.argv[5]
        sink_args = sys.argv[6]
        duration = float(sys.argv[7])
        return source, source_args, process, process_args, sink, sink_args, duration
    elif len(sys.argv) == 6:
        source = sys.argv[1]
        source_args = sys.argv[2]
        sink = sys.argv[3]
        sink_args = sys.argv[4]
        duration = float(sys.argv[5])
        process = 0
        process_args = 0


        return source, source_args, process, process_args, sink, sink_args, duration
    elif len(sys.argv) == 9:
        source = sys.argv[1]
        source_args = sys.argv[2]
        process = sys.argv[3]
        process_args = sys.argv[4:6]
        sink = sys.argv[6]
        sink_args = sys.argv[7]
        duration = float(sys.argv[8])
        return source, source_args, process, process_args, sink, sink_args, duration

def main():
    source, source_args, process, process_args, sink, sink_args, dur = parse_args()

    if source == 'sin':
        sound = sources.sin(duration=dur, freq=float(source_args))
    elif source == 'constant':
        sound = sources.constant(duration=dur, positive=bool(source_args))

        """Añadir la fuente(source)  square"""
    elif source == 'square':
        sound = sources.square(duration=dur, freq=float(source_args))
    else:
        sys.exit("Unknown source")

    if process !=0:
        if process[0] == 'ampli':
            processes.ampli(sound, factor=float(process_args[0]))
        elif process[0] == 'reduce':
            processes.reduce(sound, factor=int(process_args[0]))
        elif process[0] == 'extend':
            processes.extend(sound, factor=int(process_args[0]))
        elif process[0] == 'add':
            processes.add(sound, duration=dur, psound2=float(process_args[1]))
        else:
            sys.exit("Unknown process")
        if len(process) == 2:
            if process[1] == 'ampli':
                processes.ampli(sound, factor=float(process_args[1]))
            elif process[1] == 'reduce':
                processes.reduce(sound, factor=int(process_args[1]))
            elif process[1] == 'extend':
                processes.extend(sound, factor=int(process_args[1]))
            else:
                sys.exit("Unknown process")

    if sink == "play":
        sinks.play(sound)
    elif sink == "draw":
        sinks.draw(sound=sound, period=float(sink_args))

        """Añadir el sumidero (sink) store"""
    elif sink == "store":
        sinks.store(sound=sound, path=str(sink_args))
    else:
        sys.exit("Unknown sink")


if __name__ == '__main__':
    main()
