'''Samuel Giannotti'''
import config
import array
import sources

def ampli(sound, factor: float):
    for i in range(len(sound)):
        if sound[i] * factor >= config.max_amp:
            sound[i] = config.max_amp
        elif sound[i] * factor < config.max_amp and sound[i] * factor > -config.max_amp:
            sound[i] = int(sound[i] * factor)
    return sound

def extend(sound, factor: int):
    if factor > 0:
        for i in range (len(sound)):
            i += 1
            if i % factor == 0:
                new = ((sound[i]+sound[i+1])//2)
                sound.insert(i, new)
    return sound

def reduce(sound, factor: int):
    deletesound = sound[0::factor]
    while len(deletesound) != 0:
        for i in sound:
            if i in deletesound:
                sound.remove(i)
                deletesound.remove(i)
    return sound

def add(sound, duration, psound2: float):
    sound2 = sources.sin(duration, psound2)
    for i in range(len(sound)):
        new = (int(sound[i])+ int(sound2[i]))
        if new > config.max_amp:
            new = config.max_amp
        elif new < -config.max_amp:
            new = -config.max_amp
        sound[i]=new
    return sound

def round(sound, samples: int):
    for i in range (len(sound)):
        sound2=array.array('h', [0] * len(sound))
        sound2.insert(i, sound[i])
        if i >= samples+1:
            counter = samples
            sum = 0
            while counter != 0:
                sum = sum + sound[i-counter] + sound[i+counter]
                counter = -1
            sum = sum + sound[i]
            med = int(sum)//(int(samples*2)+1)
            sound2.insert(i, med)
    return sound2