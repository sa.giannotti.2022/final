'''Samuel Giannotti'''
"""Methods for storing sound, or playing it"""

import config
import soundfile
import sounddevice


def play(sound):
    sounddevice.play(sound, config.samples_second)
    sounddevice.wait()


def _mean(sound):
    total = 0
    for sample in sound:
        total += abs(sample)
    return int(total / len(sound))


def draw(sound, period: float):
    samples_period = int(period * config.samples_second)
    for nsample in range(0, len(sound), samples_period):
        chunk = sound[nsample: nsample + samples_period]
        mean_sample = _mean(chunk)
        stars = mean_sample // 1000
        print('*' * stars)

def store(sound, path: str):
    print('Su archivo se esta guardando, espere un momento...')
    sounddevice.wait()
    soundfile.write(path, sound, config.samples_second)
    print(f'Archivo guardado en {path}')
